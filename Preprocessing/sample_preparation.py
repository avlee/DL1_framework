""" Prepares the samples and slims them already down to the necessary amount
of variables."""

import h5py
import numpy as np
import pandas as pd
from DL1_Variables_preprocess import var_list
# from DL1_Variables import var_list
import tools_preprocess as tp


def PrepareBinnedSample(input_file, no_weights=False, dummy_weights=False):
    """Retrieves already downsampled input and applies weights for Z'
    events."""
    infile_all = h5py.File(input_file, 'r')
    label_var = "HadronConeExclTruthLabelID"
    if 'label' in list(infile_all['bjets'][:1].dtype.names):
        label_var = 'label'
    print(input_file)
    if no_weights and dummy_weights:
        var_list_mod = var_list + [label_var]
    elif no_weights:
        var_list_mod = var_list + ['weight', label_var]
    else:
        var_list_mod = var_list + ['category', label_var]
    del var_list_mod[0]
    bjets = pd.DataFrame(infile_all['bjets'][:][var_list_mod])
    cjets = pd.DataFrame(infile_all['cjets'][:][var_list_mod])
    ujets = pd.DataFrame(infile_all['ujets'][:][var_list_mod])
    X = pd.concat([bjets, cjets, ujets])
    del bjets, cjets, ujets
    if no_weights:
        if dummy_weights:
            X['weight'] = np.ones(len(X))
        return X
    N_ttbar = len(X.query("category==1"))
    N_Zprime = len(X.query("category==0"))
    Zprime_weights = round((1. / 0.65 - 1) * N_ttbar / N_Zprime, 2)
    X['category'] = X['category'].map({0.0: Zprime_weights, 1.0: 1.0})
    X = X.rename(index=str, columns={'category': 'weight'})
    return X


def RebinSample(input_file, plot_name='', odd_events=0, single_files=False,
                info_plots=True, include_all=False):
    # Allows to switch between different approaches of chosing events
    # -> pT distribution gets smoother but less statistics
    do_reduction = True
    infile_all = h5py.File(input_file, 'r')
    mini_data = infile_all['jets'][:]
    # var_list.append('eventNumber')
    print('Total Events: %i' % len(infile_all['jets']))
    # in order to make sure that training and testing events are never mixed
    if(odd_events):
        mini_data = mini_data[mini_data['eventNumber'] % 2 == 1]
    else:
        mini_data = mini_data[mini_data['eventNumber'] % 2 == 0]
    print('Events after splitting = %i' % len(mini_data))
    if not include_all:
        mini_data = mini_data[var_list]
    bjets = mini_data[mini_data['HadronConeExclTruthLabelID'] == 5]
    cjets = mini_data[mini_data['HadronConeExclTruthLabelID'] == 4]
    ujets = mini_data[mini_data['HadronConeExclTruthLabelID'] == 0]
    del mini_data
    if do_reduction:
        # ncjets = int(len(cjets) / 3)  # for pure Z' 1.3 works as well
        ncjets = int(len(cjets) / 1.5)  # for pure Z' 1.3 works as well
        bjets = np.random.choice(bjets[:], size=ncjets, replace=False)
    np.random.seed(42)  # for reproducibility
    np.random.shuffle(bjets)
    np.random.seed(42)  # for reproducibility
    np.random.shuffle(cjets)
    np.random.seed(42)  # for reproducibility
    np.random.shuffle(ujets)

    print('# bjets start:', len(bjets))
    print('# cjets start:', len(cjets))
    print('# ujets start:', len(ujets))
    print('--------------')

    if info_plots:
        print('Plotting input pT and |eta| distributions')
        tp.MakePlots(bjets, ujets, cjets, option='original',
                     plot_name=plot_name)

    print('Binning the pT and |eta| distributions')
    # bin definition
    pt_bins = np.concatenate((np.linspace(0, 600000, 351),
                              np.linspace(650000, 2000000, 28)))
    eta_bins = np.linspace(0, 2.5, 10)

    histvals_b = np.histogram2d(bjets['abs_eta_uncalib'], bjets['pt_uncalib'],
                                [eta_bins, pt_bins])
    histvals_c = np.histogram2d(cjets['abs_eta_uncalib'], cjets['pt_uncalib'],
                                [eta_bins, pt_bins])
    histvals_u = np.histogram2d(ujets['abs_eta_uncalib'], ujets['pt_uncalib'],
                                [eta_bins, pt_bins])

    b_locations_pt = np.digitize(bjets['pt_uncalib'], pt_bins) - 1
    b_locations_eta = np.digitize(bjets['abs_eta_uncalib'], eta_bins) - 1
    b_locations = zip(b_locations_pt, b_locations_eta)
    b_locations = list(b_locations)

    c_locations_pt = np.digitize(cjets['pt_uncalib'], pt_bins) - 1
    c_locations_eta = np.digitize(cjets['abs_eta_uncalib'], eta_bins) - 1
    c_locations = zip(c_locations_pt, c_locations_eta)
    c_locations = list(c_locations)

    u_locations_pt = np.digitize(ujets['pt_uncalib'], pt_bins) - 1
    u_locations_eta = np.digitize(ujets['abs_eta_uncalib'], eta_bins) - 1
    u_locations = zip(u_locations_pt, u_locations_eta)
    u_locations = list(u_locations)

    c_loc_indices = {}
    b_loc_indices = {}
    u_loc_indices = {}
    print('Grouping the bins')
    for i, x in enumerate(c_locations):
        if x not in c_loc_indices:
            c_loc_indices[x] = []
        c_loc_indices[x].append(i)

    for i, x in enumerate(b_locations):
        if x not in b_loc_indices:
            b_loc_indices[x] = []
        b_loc_indices[x].append(i)

    for i, x in enumerate(u_locations):
        if x not in u_loc_indices:
            u_loc_indices[x] = []
        u_loc_indices[x].append(i)

    cjet_indices = []
    bjet_indices = []
    ujet_indices = []
    print('Matching the bins for all flavours')
    for pt_bin_i in range(len(pt_bins) - 1):
        for eta_bin_i in range(len(eta_bins) - 1):
            loc = (pt_bin_i, eta_bin_i)

            if loc not in u_loc_indices:
                u_loc_indices[loc] = []
            if loc not in c_loc_indices:
                c_loc_indices[loc] = []
            if loc not in b_loc_indices:
                b_loc_indices[loc] = []

            nbjets = int(histvals_b[0][eta_bin_i][pt_bin_i])
            ncjets = int(histvals_c[0][eta_bin_i][pt_bin_i])
            nujets = int(histvals_u[0][eta_bin_i][pt_bin_i])

            njets = min([nbjets, ncjets, nujets])
            if do_reduction:
                njets = nbjets
            c_indices_for_bin = c_loc_indices[loc][0:njets]
            b_indices_for_bin = b_loc_indices[loc][0:njets]
            u_indices_for_bin = u_loc_indices[loc][0:njets]
            cjet_indices += c_indices_for_bin
            bjet_indices += b_indices_for_bin
            ujet_indices += u_indices_for_bin

    cjet_indices.sort()
    bjet_indices.sort()
    ujet_indices.sort()
    bjets = bjets[np.array(bjet_indices)]
    cjets = cjets[np.array(cjet_indices)]
    ujets = ujets[np.array(ujet_indices)]

    if info_plots:
        print('Plotting the final pT and |eta| distributions')
        tp.MakePlots(bjets, ujets, cjets, option='rebinned',
                     plot_name=plot_name)
    print('# bjets end:', len(bjets))
    print('# cjets end:', len(cjets))
    print('# ujets end:', len(ujets))
    print('# Total end:', len(bjets) + len(cjets) + len(ujets))
    print('--------------')

    if single_files:
        return bjets, cjets, ujets
    else:
        return np.concatenate([bjets, cjets, ujets])
