"""
 ******************************************************************************
 * Project: DL1_framework                                                     *
 * Package: atlas-flavor-tagging-tools                                        *
 *                                                                            *
 * Description:                                                               *
 *      downsampling tools for DL1 tagger                                     *
 *                                                                            *
 * Authors:                                                                   *
 *      ATLAS flavour tagging group, CERN, Geneva                             *
 *                                                                            *
 ******************************************************************************
"""
from __future__ import print_function
import h5py
import numpy as np
from numpy.lib.recfunctions import append_fields
import os
import json
import argparse
from collections import namedtuple
import tools_preprocess as tp


def GetParser():
    """Argparse option for downsampling script."""
    parser = argparse.ArgumentParser(description=""" Options for making the
                                     hybrid sample.""")

    parser.add_argument('-c', '--config_file', type=str,
                        required=True,
                        help="""Enter the name ofthe config file to create the
                        hybrid sample.""")
    args = parser.parse_args()

    with open(args.config_file) as json_file:
        config = json.load(json_file, object_hook=lambda d: namedtuple(
            'X', d.keys())(*d.values()))

    return config


def GetPtCuts(jets, config, sample='ttbar'):
    if sample == 'ttbar':
        if config.bhad_pTcut is None:
            bhad_pTcut = config.pT_max
        else:
            bhad_pTcut = config.bhad_pTcut
        if config.bhad_pTcut is None:
            pTcut = config.pT_max
        else:
            pTcut = config.pTcut
        indices_to_remove = np.where(
            ((jets["HadronConeExclTruthLabelID"] == 5) & (
                jets["GhostBHadronsFinalPt"] > bhad_pTcut)) |
            ((jets["HadronConeExclTruthLabelID"] < 5) & (
                jets["pt_uncalib"] > pTcut)) |
            ((jets["secondaryVtx_m"] > 25000) &
             (jets["secondaryVtx_m"] == jets["secondaryVtx_m"])) |
            ((jets["secondaryVtx_E"] > 1e8) &
             (jets["secondaryVtx_E"] == jets["secondaryVtx_E"])) |
            (jets["vr_overlap"] == 1) |
            (jets["jetPtRank"] > 3)
        )[0]
    elif sample == "Zprime":
        if config.bhad_pTcut is None:
            bhad_pTcut = 0.
        else:
            bhad_pTcut = config.bhad_pTcut
        if config.bhad_pTcut is None:
            pTcut = 0.
        else:
            pTcut = config.pTcut
        indices_to_remove = np.where(
            ((jets["HadronConeExclTruthLabelID"] == 5) & (
                jets["GhostBHadronsFinalPt"] < bhad_pTcut)) |
            ((jets["HadronConeExclTruthLabelID"] < 5) & (
                jets["pt_uncalib"] < pTcut)) |
            (jets["pt_uncalib"] > config.pT_max) |
            ((jets["secondaryVtx_m"] > 25000) &
             (jets["secondaryVtx_m"] == jets["secondaryVtx_m"])) |
            ((jets["secondaryVtx_E"] > 1e8) &
             (jets["secondaryVtx_E"] == jets["secondaryVtx_E"])) |
            (jets["vr_overlap"] == 1) |
            (jets["jetPtRank"] > 1)

        )[0]
    else:
        print("Chose either 'ttbar' or 'Zprime' as argument for sample")
        return 1

    return indices_to_remove


def GetCuts(jets):
    indices_to_remove = np.where(
        ((jets["JetFitter_deltaR"] > 0.6) &
         (jets["JetFitter_deltaR"] == jets["JetFitter_deltaR"])) |
        ((jets["secondaryVtx_m"] > 500000) &
         (jets["secondaryVtx_m"] == jets["secondaryVtx_m"])) |
        ((jets["secondaryVtx_E"] > 1.0e10) &
         (jets["secondaryVtx_E"] == jets["secondaryVtx_E"])) |
        ((jets["softMuon_pt"] > 0.5e9) &
         (jets["softMuon_pt"] == jets["softMuon_pt"])) |
        ((jets["softMuon_momentumBalanceSignificance"] > 50) &
         (jets["softMuon_momentumBalanceSignificance"] ==
          jets["softMuon_momentumBalanceSignificance"]))
    )[0]
    return indices_to_remove


def DownSampling(bjets, cjets, ujets):
    pt_bins = np.concatenate((np.linspace(0, 600000, 351),
                              np.linspace(650000, 6000000, 84)))
    eta_bins = np.linspace(0, 2.5, 10)

    histvals_b = np.histogram2d(bjets['abs_eta_uncalib'], bjets['pt_uncalib'],
                                [eta_bins, pt_bins])
    histvals_c = np.histogram2d(cjets['abs_eta_uncalib'], cjets['pt_uncalib'],
                                [eta_bins, pt_bins])
    histvals_u = np.histogram2d(ujets['abs_eta_uncalib'], ujets['pt_uncalib'],
                                [eta_bins, pt_bins])

    b_locations_pt = np.digitize(bjets['pt_uncalib'], pt_bins) - 1
    b_locations_eta = np.digitize(bjets['abs_eta_uncalib'], eta_bins) - 1
    b_locations = zip(b_locations_pt, b_locations_eta)
    b_locations = list(b_locations)

    c_locations_pt = np.digitize(cjets['pt_uncalib'], pt_bins) - 1
    c_locations_eta = np.digitize(cjets['abs_eta_uncalib'], eta_bins) - 1
    c_locations = zip(c_locations_pt, c_locations_eta)
    c_locations = list(c_locations)

    u_locations_pt = np.digitize(ujets['pt_uncalib'], pt_bins) - 1
    u_locations_eta = np.digitize(ujets['abs_eta_uncalib'], eta_bins) - 1
    u_locations = zip(u_locations_pt, u_locations_eta)
    u_locations = list(u_locations)

    c_loc_indices = {}
    b_loc_indices = {}
    u_loc_indices = {}
    print('Grouping the bins')
    for i, x in enumerate(c_locations):
        if x not in c_loc_indices:
            c_loc_indices[x] = []
        c_loc_indices[x].append(i)

    for i, x in enumerate(b_locations):
        if x not in b_loc_indices:
            b_loc_indices[x] = []
        b_loc_indices[x].append(i)

    for i, x in enumerate(u_locations):
        if x not in u_loc_indices:
            u_loc_indices[x] = []
        u_loc_indices[x].append(i)

    cjet_indices = []
    bjet_indices = []
    ujet_indices = []
    print('Matching the bins for all flavours')
    for pt_bin_i in range(len(pt_bins) - 1):
        for eta_bin_i in range(len(eta_bins) - 1):
            loc = (pt_bin_i, eta_bin_i)

            if loc not in u_loc_indices:
                u_loc_indices[loc] = []
            if loc not in c_loc_indices:
                c_loc_indices[loc] = []
            if loc not in b_loc_indices:
                b_loc_indices[loc] = []

            nbjets = int(histvals_b[0][eta_bin_i][pt_bin_i])
            ncjets = int(histvals_c[0][eta_bin_i][pt_bin_i])
            nujets = int(histvals_u[0][eta_bin_i][pt_bin_i])

            njets = min([nbjets, ncjets, nujets])
#             if do_reduction:
#                 njets = nbjets
            c_indices_for_bin = c_loc_indices[loc][0:njets]
            b_indices_for_bin = b_loc_indices[loc][0:njets]
            u_indices_for_bin = u_loc_indices[loc][0:njets]
            cjet_indices += c_indices_for_bin
            bjet_indices += b_indices_for_bin
            ujet_indices += u_indices_for_bin

    cjet_indices.sort()
    bjet_indices.sort()
    ujet_indices.sort()
    bjets = bjets[np.array(bjet_indices)]
    cjets = cjets[np.array(cjet_indices)]
    ujets = ujets[np.array(ujet_indices)]
    return bjets, cjets, ujets


def GetNs(config):
    # jets_tot = int(config.Njets * 3 * (1 / config.ttbar_frac - 1) +
    # config.Njets * 3)
    if config.ttbar_frac > 0:
        nZ = int(config.Njets * 3 * (1 / config.ttbar_frac - 1)
                 ) // config.iterations
        ncjets = int(2.3 * config.Njets) // config.iterations
        nujets = int(2.7 * config.Njets) // config.iterations
        Njets = int(config.Njets) // config.iterations
    else:
        nZ = int(config.Njets) // config.iterations
        ncjets = nujets = Njets = 0

    N_list = []
    for x in range(config.iterations + 1):
        N_dict = {"nZ": nZ * x,
                  "nbjets": Njets * x,
                  "ncjets": ncjets * x,
                  "nujets": nujets * x
                  }
        N_list.append(N_dict)
    return N_list


def main():
    config = GetParser()
    N_list = GetNs(config)

    f_Z = h5py.File(os.path.join(config.file_path, config.f_z), 'r')
    f_ttb_bjets = h5py.File(os.path.join(config.file_path, config.f_ttb_bjets),
                            'r')
    f_ttb_cjets = h5py.File(os.path.join(config.file_path, config.f_ttb_cjets),
                            'r')
    f_ttb_ujets = h5py.File(os.path.join(config.file_path, config.f_ttb_ujets),
                            'r')

    for x in range(config.iterations):
        print("Iteration", x + 1, "of", config.iterations)
        vec_Z = f_Z['jets'][N_list[x]["nZ"]:N_list[x + 1]["nZ"]]
        vec_Z = append_fields(vec_Z, "category",
                              np.zeros(len(vec_Z)),
                              dtypes='<f4', asrecarray=True)
        vec_ttb_bjets = f_ttb_bjets['jets'][N_list[x]["nbjets"]:N_list[x + 1]
                                            ["nbjets"]]
        vec_ttb_bjets = append_fields(vec_ttb_bjets, "category",
                                      np.ones(len(vec_ttb_bjets)),
                                      dtypes='<f4', asrecarray=True)
        vec_ttb_cjets = f_ttb_cjets['jets'][N_list[x]["ncjets"]:N_list[x + 1]
                                            ["ncjets"]]
        vec_ttb_cjets = append_fields(vec_ttb_cjets, "category",
                                      np.ones(len(vec_ttb_cjets)),
                                      dtypes='<f4', asrecarray=True)
        vec_ttb_ujets = f_ttb_ujets['jets'][N_list[x]["nujets"]:N_list[x + 1]
                                            ["nujets"]]
        vec_ttb_ujets = append_fields(vec_ttb_ujets, "category",
                                      np.ones(len(vec_ttb_ujets)),
                                      dtypes='<f4', asrecarray=True)
        vec_Z = np.delete(vec_Z, GetPtCuts(vec_Z, config, 'Zprime'), 0)
        vec_ttb_bjets = np.delete(vec_ttb_bjets, GetPtCuts(vec_ttb_bjets,
                                                           config), 0)
        vec_ttb_cjets = np.delete(vec_ttb_cjets, GetPtCuts(vec_ttb_cjets,
                                                           config), 0)
        vec_ttb_ujets = np.delete(vec_ttb_ujets, GetPtCuts(vec_ttb_ujets,
                                                           config), 0)

        print("starting downsampling")
        b, c, u = DownSampling(
            np.concatenate([vec_Z[vec_Z["HadronConeExclTruthLabelID"] == 5],
                            vec_ttb_bjets]),
            np.concatenate([vec_Z[vec_Z["HadronConeExclTruthLabelID"] == 4],
                            vec_ttb_cjets]),
            np.concatenate([vec_Z[vec_Z["HadronConeExclTruthLabelID"] == 0],
                            vec_ttb_ujets])
        )
        ttfrac = float(len(b[b["category"] == 1]) + len(c[c["category"] == 1]) +
                  len(u[u["category"] == 1])) / float(len(b) + len(c) + len(u))
        print("ttbar fraction:", ttfrac)
        out_file = config.outfile_name
        if config.iterations > 1:
            idx = out_file.index(".h5")
            inserttxt = "-file%i_%i" % (x + 1, config.iterations)
            out_file = out_file[:idx] + inserttxt + out_file[idx:]
        print("saving file:", out_file)
        h5f = h5py.File(out_file, 'w')
        h5f.create_dataset('bjets', data=b)
        h5f.create_dataset('cjets', data=c)
        h5f.create_dataset('ujets', data=u)
        h5f.close()
        print("Plotting ...")
        tp.MakePlots(b, u, c, plot_name=config.plot_name, option=str(x),
                     binning={"pt_uncalib": 200, "abs_eta_uncalib": 200})


if __name__ == '__main__':
    main()
