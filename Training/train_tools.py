import os
import numpy as np
from numpy.lib.recfunctions import repack_fields
try:
    from numpy.lib.recfunctions import structured_to_unstructured
except ImportError:
    pass
import json
import h5py
import pandas as pd
from keras.utils import np_utils
# from tensorflow.keras.callbacks import Callback
# from tensorflow.keras.utils import Sequence
# from tensorflow.keras.models import load_model
# from tensorflow.keras.utils import CustomObjectScope
# from tensorflow.keras.initializers import glorot_uniform
from keras.callbacks import Callback
from keras.utils import Sequence
from keras.models import load_model
from keras.utils import CustomObjectScope
from keras.initializers import glorot_uniform


def Gen_default_dict(scale_dict):
    """Generates default value dictionary from scale/shift dictionary."""
    default_dict = {}
    for elem in scale_dict:
        if 'isDefaults' in elem['name']:
            continue
        default_dict[elem['name']] = elem['default']
    return default_dict


def Apply_scale(file, dict_file, variable_config="DL1_Variables_mu.json",
                vr_overlap=False):
    """Takes test file and applies scale and offset correction defined in json
    file."""
    file = h5py.File(file, 'r')
    with open(variable_config) as vardict:
        var_list = json.load(vardict)[:]
    var_list.remove('HadronConeExclTruthLabelID')
    bjet = pd.DataFrame(file['bjets'][:][var_list])
    cjet = pd.DataFrame(file['cjets'][:][var_list])
    ujet = pd.DataFrame(file['ujets'][:][var_list])
    if vr_overlap:  # TODO:  not working yet!! -> vr_overlap needs to be in varlist
        bjet.query("vr_overlap==0", inplace=True)
        cjet.query("vr_overlap==0", inplace=True)
        ujet.query("vr_overlap==0", inplace=True)
    with open(dict_file, 'r') as infile:
        scale_dict = json.load(infile)
    bjet.replace([np.inf, -np.inf], np.nan, inplace=True)
    cjet.replace([np.inf, -np.inf], np.nan, inplace=True)
    ujet.replace([np.inf, -np.inf], np.nan, inplace=True)
    # Replace NaN values with default values from default dictionary
    default_dict = Gen_default_dict(scale_dict)
    bjet.fillna(default_dict, inplace=True)
    cjet.fillna(default_dict, inplace=True)
    ujet.fillna(default_dict, inplace=True)
    # scale and shift distribution
    for elem in scale_dict:
        if 'isDefaults' in elem['name']:
            continue
        if elem['name'] not in var_list:
            continue
        else:
            bjet[elem['name']] = ((bjet[elem['name']] - elem['shift']) /
                                  elem['scale'])
            cjet[elem['name']] = ((cjet[elem['name']] - elem['shift']) /
                                  elem['scale'])
            ujet[elem['name']] = ((ujet[elem['name']] - elem['shift']) /
                                  elem['scale'])

    return bjet.values, cjet.values, ujet.values


def Apply_scale_origin(file, dict_file=None,
                       variable_config="DL1_Variables_mu.json",
                       get_pt_eta=False, vr_overlap=False):
    """Takes test file and applies scale and offset correction defined in json
    file.
    Inputs:
    - file              file name of the hdf5 file
    - dict_file         dictionary file conaining the shift, scale and default
                        values
    - variable_config   json file name in which the order of the used variables
                        is defined
    - get_pt_eta        if set to true only the pt and eta pandas DataFrame are
                        returned

    """
    file = h5py.File(file, 'r')
    if get_pt_eta:
        var_list = ['eventNumber', 'HadronConeExclTruthLabelID', 'pt_uncalib',
                    'abs_eta_uncalib']
    else:
        with open(variable_config) as vardict:
            var_list = json.load(vardict)[:]
        var_list.append('eventNumber')
    if vr_overlap:
        var_list.append('vr_overlap')

    jets = pd.DataFrame(file['jets'][:][var_list])
    if vr_overlap:
        jets.query("vr_overlap==0", inplace=True)
        var_list.remove('vr_overlap')
    var_list.remove('HadronConeExclTruthLabelID')
    var_list.remove('eventNumber')
    print('Found', len(var_list), 'variables')
    bjet = jets.query("HadronConeExclTruthLabelID==5 & \
                      eventNumber %2 == 1").copy()[var_list]
    cjet = jets.query("HadronConeExclTruthLabelID==4 & \
                      eventNumber %2 == 1").copy()[var_list]
    ujet = jets.query("HadronConeExclTruthLabelID==0 & \
                      eventNumber %2 == 1").copy()[var_list]
    del jets
    if get_pt_eta:
        return bjet, cjet, ujet
    with open(dict_file, 'r') as infile:
        scale_dict = json.load(infile)
    bjet.replace([np.inf, -np.inf], np.nan, inplace=True)
    cjet.replace([np.inf, -np.inf], np.nan, inplace=True)
    ujet.replace([np.inf, -np.inf], np.nan, inplace=True)
    bjet.replace({"JetFitter_deltaR": {15.556349: np.nan}}, inplace=True)
    cjet.replace({"JetFitter_deltaR": {15.556349: np.nan}}, inplace=True)
    ujet.replace({"JetFitter_deltaR": {15.556349: np.nan}}, inplace=True)
    # Replace NaN values with default values from default dictionary
    default_dict = Gen_default_dict(scale_dict)
    bjet.fillna(default_dict, inplace=True)
    cjet.fillna(default_dict, inplace=True)
    ujet.fillna(default_dict, inplace=True)
    # scale and shift distribution
    for elem in scale_dict:
        if 'isDefaults' in elem['name']:
            continue
        if elem['name'] not in var_list:
            continue
        else:
            bjet[elem['name']] = ((bjet[elem['name']] - elem['shift']) /
                                  elem['scale'])
            cjet[elem['name']] = ((cjet[elem['name']] - elem['shift']) /
                                  elem['scale'])
            ujet[elem['name']] = ((ujet[elem['name']] - elem['shift']) /
                                  elem['scale'])

    return bjet.values, cjet.values, ujet.values


def Get_DictInfo(ttfrac=0.85, dict_path="./scale_dicts",
                 sample_path="./samples"):
    with open('/home/mg294/notebooks/dl1_jupyter/train_file_dict.json') as dicti:
        train_file_dict = json.load(dicti)
    dict_x = train_file_dict[str(ttfrac)]
    dict_x["sample_name"] = "%s/%s" % (sample_path, dict_x["sample_name"])
    dict_x["scale_dict"] = "%s/%s" % (dict_path, dict_x["scale_dict"])
    return dict_x


def Get_TrainingSample(file, do_weights=False,
                       variable_config="DL1_Variables_mu.json"):
    print('reading in file: %s' % file)
    file = h5py.File(file, 'r')
    train_bjet = file['train_processed_bjets'][:]
    train_cjet = file['train_processed_cjets'][:]
    train_ujet = file['train_processed_ujets'][:]
    with open(variable_config) as vardict:
        var_names = json.load(vardict)[:]
#     var_names = list(train_bjet.dtype.names)
    var_names.remove('HadronConeExclTruthLabelID')
#     var_names.remove('weight')
    print('concatenating flavour samples')
    X_train = np.concatenate((train_ujet, train_cjet, train_bjet))
    y_train = np.concatenate((np.zeros(len(train_ujet)),
                              np.ones(len(train_cjet)),
                              2 * np.ones(len(train_bjet))))
    del train_bjet, train_cjet, train_ujet
    Y_train = np_utils.to_categorical(y_train, 3)
    if do_weights:
        weights = X_train['weight']
    X_train = repack_fields(X_train[var_names])
    X_train = X_train.view(np.float64).reshape(X_train.shape + (-1,))
    print(X_train.shape)
    rng_state = np.random.get_state()
    # shuffle the arrays
    print('suffle arrays')
    np.random.shuffle(X_train)
    np.random.set_state(rng_state)
    np.random.shuffle(Y_train)
    assert X_train.shape[1] == len(var_names)
    if do_weights:
        np.random.set_state(rng_state)
        np.random.shuffle(weights)
        return X_train, Y_train, weights
    return X_train, Y_train


def Get_TestSamples(file, scale_dict, variable_config="DL1_Variables_mu.json",
                    origin=False, vr_overlap=False):
    if origin:
        test_bjet, test_cjet, test_ujet = Apply_scale_origin(file,
                                                             scale_dict,
                                                             variable_config=variable_config,
                                                             vr_overlap=vr_overlap)
    else:
        test_bjet, test_cjet, test_ujet = Apply_scale(file, scale_dict,
                                                      variable_config=variable_config,
                                                      vr_overlap=vr_overlap)
    X_test = np.concatenate((test_ujet, test_cjet, test_bjet))
    y_test = np.concatenate((np.zeros(len(test_ujet)),
                             np.ones(len(test_cjet)),
                             2 * np.ones(len(test_bjet))))
    Y_test = np_utils.to_categorical(y_test, 3)
    return X_test, Y_test


def GetRejection(y_pred, y_true):
    """Calculates the c and light rejection for 77% WP and 0.018 c-fraction."""
    b_index, c_index, u_index = 2, 1, 0
    cfrac = 0.018
    target_beff = 0.77
    y_true = np.argmax(y_true, axis=1)
    b_jets = y_pred[y_true == b_index]
    c_jets = y_pred[y_true == c_index]
    u_jets = y_pred[y_true == u_index]
    bscores = np.log(b_jets[:, b_index] / (cfrac * b_jets[:, c_index] +
                                           (1 - cfrac) * b_jets[:, u_index]))
    cutvalue = np.percentile(bscores, 100.0 * (1.0 - target_beff))

    c_eff = len(c_jets[np.log(c_jets[:, b_index] / (cfrac * c_jets[:, c_index]
                                                    + (1 - cfrac) *
                                                    c_jets[:, u_index])) >
                       cutvalue]) / float(len(c_jets))
    u_eff = len(u_jets[np.log(u_jets[:, b_index] / (cfrac *
                                                    u_jets[:, c_index] +
                                                    (1 - cfrac) *
                                                    u_jets[:, u_index])) >
                       cutvalue]) / float(len(u_jets))

    if c_eff == 0 or u_eff == 0:
        return -1, -1
    return 1. / c_eff, 1. / u_eff


class MyCallback(Callback):
    def __init__(self, X_valid=0, Y_valid=0, log_file=None, verbose=False,
                 model_name='test', X_valid_add=None, Y_valid_add=None):
        self.X_valid = X_valid
        self.Y_valid = Y_valid
        self.X_valid_add = X_valid_add
        self.Y_valid_add = Y_valid_add
        self.result = []
        self.log = open(log_file, 'w') if log_file else None
        self.verbose = verbose
        self.model_name = model_name
        os.system("mkdir -p %s" % self.model_name)
        self.dict_list = []

    def on_epoch_end(self, epoch, logs=None):
        self.model.save('%s/model_epoch%i.h5' % (self.model_name, epoch))
        y_pred = self.model.predict(self.X_valid, batch_size=5000)
        c_rej, u_rej = GetRejection(y_pred, self.Y_valid)
        if self.X_valid_add is not None:
            add_loss, add_acc = self.model.evaluate(self.X_valid_add,
                                                    self.Y_valid_add,
                                                    batch_size=5000)
            y_pred_add = self.model.predict(self.X_valid_add, batch_size=5000)
            c_rej_add, u_rej_add = GetRejection(y_pred_add, self.Y_valid_add)
        dict_epoch = {
            "epoch": epoch,
            "loss": logs['loss'],
            "acc": logs['acc'],
            "val_loss": logs['val_loss'],
            "val_acc": logs['val_acc'],
            "c_rej": c_rej,
            "u_rej": u_rej,
            "val_loss_add": add_loss if add_loss else None,
            "val_acc_add": add_acc if add_acc else None,
            "c_rej_add": c_rej_add if c_rej_add else None,
            "u_rej_add": u_rej_add if u_rej_add else None
        }

        self.dict_list.append(dict_epoch)
        with open('%s/DictFile.json' % self.model_name, 'w') as outfile:
            json.dump(self.dict_list, outfile, indent=4)
        #         print('accuracy:', logs['acc'])
        # loss_list = [logs['loss']]
        # acc_list = [logs['acc']]
        # eval_loss = self.model.evaluate(self.test_data, self.test_label,
        #                                 batch_size=300, verbose=0)
        #
        # loss_file = "%s/LossFile_Epoch%i.h5" % (self.model_name, epoch)
        # h5f = h5py.File(loss_file, 'w')
        # h5f.create_dataset('train_loss_list', data=loss_list)
        # h5f.create_dataset('eval_loss_list', data=eval_loss)
        # h5f.create_dataset('train_acc_list', data=acc_list)
        # h5f.close()

    def on_train_end(self, logs=None):
        if self.log:
            self.log.close()


def EvalJson(model_name='/data/DL1_models/batch_normalization'):
    with open('%s/DictFile.json' % model_name) as dicfile:
        dict_list = json.load(dicfile)
    val_loss = 99
    epoch = 0
    print(len(dict_list), "epochs")
    for elem in dict_list:
        if elem['val_loss'] < val_loss:
            val_loss = elem['val_loss']
            epoch = elem['epoch']
    print("less val_loss:", val_loss)
    print("in epoch:", epoch)
    return epoch


def Save_ROC(model, test_b, test_c, test_u, file_type, ttfrac,
             save_path="./ROCs", appendi=''):
    crej, urej = GetRejCurve(model, test_b, test_c, test_u)
    name_crej = "%s/ROC_hybrid-%i_%s%s-crej.npy" % (save_path, ttfrac,
                                                    file_type, appendi)
    print("save crej as:", name_crej)
    name_urej = "%s/ROC_hybrid-%i_%s%s-urej.npy" % (save_path, ttfrac,
                                                    file_type, appendi)
    print("save urej as:", name_urej)
    np.save(name_crej, crej)
    np.save(name_urej, urej)


class DataGenerator(Sequence):
    """Generates data for Keras."""

    def __init__(self, X, Y, sample_weight=None, batch_size=300,
                 shuffle=False):
        """Initialisation."""
        self.batch_size = batch_size
        self.x = X
        self.y = Y
        self.sample_weight = sample_weight
#         self.shuffle = shuffle
#         self.on_epoch_end()

    def __len__(self):
        """Denotes the number of batches per epoch."""
        return int(np.ceil(len(self.x) / float(self.batch_size)))

    def __getitem__(self, idx):
        """Generate one batch of data."""
        batch_x = self.x[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.y[idx * self.batch_size:(idx + 1) * self.batch_size]
        if self.sample_weight is not None:
            batch_weight = self.sample_weight[idx * self.batch_size:(idx + 1)
                                              * self.batch_size]
            return batch_x, batch_y, batch_weight

        return batch_x, batch_y


class DataGenerator_ReadFile(Sequence):
    """Generates data for Keras."""

    def __init__(self, X, Y, sample_weight=None, batch_size=300):
        """Initialisation."""
        self.batch_size = batch_size
        self.x = X
        self.y = Y
        self.sample_weight = sample_weight

    def __len__(self):
        """Denotes the number of batches per epoch."""
        return int(np.ceil(len(self.x) / float(self.batch_size)))

    def __getitem__(self, idx):
        """Generate one batch of data."""
        batch_x = self.x[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.y[idx * self.batch_size:(idx + 1) * self.batch_size]
        if self.sample_weight is not None:
            batch_weight = self.sample_weight[idx * self.batch_size:(idx + 1)
                                              * self.batch_size]
            return batch_x, batch_y, batch_weight

        return batch_x, batch_y


def GetEfficiency(b_jets, c_jets, u_jets, total_c_jets, total_u_jets,
                  b_frac, target_beff=0.77, cutvalue=None):
    b_index, c_index, u_index = 2, 1, 0

    bscores = np.log(b_jets[:, b_index] / (b_frac * b_jets[:, c_index] +
                                           (1-b_frac)*b_jets[:, u_index]))
    if cutvalue is None:
        cutvalue = np.percentile(bscores, 100.0 * (1.0 - target_beff))

    c_eff = len(c_jets[np.log(c_jets[:, b_index]/(b_frac*c_jets[:, c_index]
                                                  + (1 - b_frac) *
                                                  c_jets[:, u_index])) >
                       cutvalue]) / float(total_c_jets)
    u_eff = len(u_jets[np.log(u_jets[:, b_index] / (b_frac *
                                                    u_jets[:, c_index] +
                                                    (1 - b_frac) *
                                                    u_jets[:, u_index])) >
                       cutvalue]) / float(total_u_jets)

    return c_eff, u_eff, cutvalue


def MakeRejection(b_jets, c_jets, u_jets, fixed_fb=False, fb=0.08,
                  cutvalue=None):
    number_of_x_steps = 500
    c_eff_list = []
    u_eff_list = []
    cut_x_list = []
    cut_y_list = []

    total_c_jets = len(c_jets)
    total_u_jets = len(u_jets)

    percent_shown = 0

    if fixed_fb:
        c_eff, u_eff, _ = GetEfficiency(b_jets, c_jets, u_jets, total_c_jets,
                                        total_u_jets, fb, cutvalue=cutvalue)
        return 1 / c_eff, 1 / u_eff

    for b_frac in np.linspace(0, 1, number_of_x_steps):

        percent = int(((b_frac) * 100.0) / (1.0))

        if(percent % 10 == 0 and percent > percent_shown):
            print(str(percent) + ' percent done')
            percent_shown = percent

        c_eff, u_eff, cutval = GetEfficiency(b_jets, c_jets, u_jets,
                                             total_c_jets, total_u_jets,
                                             b_frac, cutvalue=cutvalue)
        if c_eff == 0 or u_eff == 0:
            continue
        c_eff_list.append(c_eff)

        u_eff_list.append(u_eff)
        cut_x_list.append(b_frac)
        cut_y_list.append(cutval)
        if b_frac > 0.08 and b_frac < 0.09:
            print(1/c_eff, 1/u_eff)

    c_rej = [1 / x for x in c_eff_list]
    u_rej = [1 / x for x in u_eff_list]

    return c_rej, u_rej, cut_x_list, cut_y_list


def GetRejCurve(model_file, bjets, cjets, ujets, onlyRej=True, onlyPred=False,
                fixed_fc=False, fc=0.08, b_effs=[0.59, 1, 100]):
    with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
        model = load_model(model_file)
    bjets_pred = model.predict(bjets, batch_size=5000, verbose=1)
    cjets_pred = model.predict(cjets, batch_size=5000, verbose=1)
    ujets_pred = model.predict(ujets, batch_size=5000, verbose=1)
    if onlyPred:
        return bjets_pred, cjets_pred, ujets_pred
    elif fixed_fc:
        rej_list_c = []
        rej_list_u = []
        for x in np.linspace(*b_effs):
            cutvalue = GetWP(bjets_pred, target_beff=x, b_frac=fc)
            crej, urej = MakeRejection(bjets_pred, cjets_pred, ujets_pred,
                                       True, cutvalue=cutvalue, fb=fc)
            rej_list_c.append(crej)
            rej_list_u.append(urej)
        return np.array(rej_list_c), np.array(rej_list_u)

    c_rej, u_rej, fraclist, cutlist = MakeRejection(bjets_pred, cjets_pred,
                                                    ujets_pred)
    if onlyRej:
        return c_rej, u_rej
    else:
        return c_rej, u_rej, fraclist, cutlist


def GetRejCurve_ntuple(bjets_pred, cjets_pred, ujets_pred,
                       fc=0.08, b_effs=[0.59, 1, 100]):
    """*jets_pred are ndarrays with the predictions [pb, pc, pb]"""

    rej_list_c = []
    rej_list_u = []
    for x in np.linspace(*b_effs):
        cutvalue = GetWP(bjets_pred, target_beff=x, b_frac=fc)
        crej, urej = MakeRejection(bjets_pred, cjets_pred, ujets_pred,
                                   True, cutvalue=cutvalue, fb=fc)
        rej_list_c.append(crej)
        rej_list_u.append(urej)
    return np.array(rej_list_c), np.array(rej_list_u)


def MakeRejection_MV2(b_jets, c_jets, u_jets, target_beff=0.77, cutvalue=None):

    if cutvalue is None:
        cutvalue = np.percentile(b_jets, 100.0 * (1.0 - target_beff))

    c_eff = len(c_jets[c_jets > cutvalue]) / float(len(c_jets))
    u_eff = len(u_jets[u_jets > cutvalue]) / float(len(u_jets))

    return 1 / c_eff, 1 / u_eff


from scipy.interpolate import pchip
from matplotlib import gridspec
import matplotlib.pyplot as plt

eff_err = lambda x, N: np.sqrt( x*(1-x) / N)


def plotROCRatio(teffs, beffs, labels, title='',text='',ylabel='Background rejection',
                 tag='', figDir='../figures',subDir='mc16d',
                 styles=None,colors=None, xmin=.6,ymax=1e3,legFontSize=10,
                 rrange=None, rlabel='Ratio', binomialErrors=False, nTest=None, plot_name=None, alabel=None,
                legcols=2, flipRatio=False, labelpad=None):
    '''

    Plot the ROC curves with binomial errors with the ratio plot in a subpanel
    underneath. This function all accepts the same inputs as plotROC, and the
    additional ones are listed below.

    Addtional Inputs:
    - rrange: The range on the y-axis for the ratio panel
    - rlabel: The label for the y-axis for the ratio panel
    - binomialErrors: whether to include binomial errors for the rejection curves
    '''

    # The points that I'm going to c.f. the ratio over
#     xx = np.linspace(0.6,1,101)
    xx = np.linspace(xmin,1,101)

    if binomialErrors and nTest is None:
        print("Error: Requested binomialErrors, but did not pass nTest. Will NOT plot rej errors.")
        binomialErrors = False

    if styles is None:
        styles = ['-' for i in teffs]
    if colors is None:
        colors = ['C{}'.format(i) for i in range(len(teffs))]

    # Define the figure with two subplots of unequal sizes
    fig = plt.figure(figsize=(6,5))
    gs = gridspec.GridSpec(5,1)
    ax1 = fig.add_subplot(gs[:4,0])
    ax2 = fig.add_subplot(gs[4:,0],sharex=ax1)

    for i, (teff, beff, label, style, color) in enumerate(zip(teffs, beffs, labels, styles, colors)):

        # Mask the rejections that are 0
        nonzero = (beff != 0)
        x = teff[nonzero]
        y = np.divide(1,beff[nonzero])

        if binomialErrors:

            yerr = np.power(y,2) * eff_err(beff[nonzero],nTest)

            y1 = y - yerr
            y2 = y + yerr

            #p.fill_between(x,y1,y2)
            ax1.fill_between(x,y1,y2, color=color,label=label, linestyle=style) #, alpha=0.5)

        else:
            ax1.semilogy(x, y, style, color=color, label=label)

        f = pchip(x,y)

        if i == 0:
            f0 = f

            if binomialErrors:
                # Use a grey contour to show our uncertainty in the value of 1
                ax2.fill_between(x,1-yerr/y,1+yerr/y,color='grey',alpha=0.5)

        else:
            if flipRatio:
                ax2.plot(xx,  f0(xx) / f(xx), style, color=color)
            else:
                ax2.plot(xx, f(xx) / f0(xx), style, color=color)

    # Add a line indicating where the ratio=1
    ax2.plot([xmin,1],[1,1],'k--')

    # Add axes, titels and the legend
    ax2.set_xlabel('$b$-efficiency',fontsize=14)
    ax1.set_ylabel(ylabel,fontsize=14)
    ax1.set_yscale('log')
    ax2.set_ylabel(rlabel, labelpad=labelpad)
    ax1.text(xmin,ymax,text,horizontalalignment='left', verticalalignment='bottom')
    ax1.set_title(title)
    ax2.grid()
    ax1.grid()
    plt.setp(ax1.get_xticklabels(), visible=False)

    # Print label
    if alabel is not None:
        ax1.text(**alabel)

    # Set the axes to be the same as those used in the pub note
    ax1.set_xlim(xmin,1)
    ax1.set_ylim(1,ymax)

    ax2.set_xlim(xmin,1)
    if rrange is not None:
        ax2.set_ylim(rrange)
    ax1.legend(loc='best',fontsize=legFontSize, ncol=legcols)

    if len(tag) != 0:
        plt.savefig('{}/{}/rocRatio_{}.pdf'.format(figDir,subDir,tag),bbox_inches='tight')
    if plot_name is not None:
        plt.savefig(plot_name, transparent=True)
    plt.show()



def GetWP(df_b, target_beff, b_index=2, c_index=1, u_index=0, b_frac=0.08):
    bscores = np.log(df_b[:, b_index] / (b_frac * df_b[:, c_index] +
                                           (1-b_frac)*df_b[:, u_index]))
    return np.percentile(bscores, 100.0 * (1.0 - target_beff))
